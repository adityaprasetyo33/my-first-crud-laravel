@extends('adminlte.master')

@section('content')
<div>
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/pertanyaan/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">judul</label>
            <input type="text" class="form-control" name="judul" value="{{$post->judul}}" id="judul"
                placeholder="Masukkan judul">
            @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">isi</label>
            <input type="text" class="form-control" name="isi" value="{{$post->isi}}" id="isi"
                placeholder="Masukkan isi">
            @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection
