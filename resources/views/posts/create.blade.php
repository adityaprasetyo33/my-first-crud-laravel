@extends('adminlte.master')
@section('content')
<h2>Tambah Data</h2>
<form action="/pertanyaan" method="POST">
    @csrf
    <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
        @error('judul')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="isi">isi</label>
        <input type="text" class="form-control" name="isi" id="isi" placeholder="Masukkan isi">
        @error('isi')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
</div>
@error('isi')
<div class=”alert alert-danger”>
    {{ $message }}
</div>
@enderror
@endsection
